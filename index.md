# **N-sYnC** justiN chris joeY lanCe jc

## **Food**

### Burgers
- King of Kobe
- White Castle (think Micky Ds but four of them and 1/4th the size)
- Shake Shack (some say it's the east coast version of in-n-out, I personally don't think so)
- Bareburgers (like shake shack)

### Chinese
- Wah Fung, one of the chinatown kings of cheap eats 
- King's Kitchen
- Lanzhou handpulled noodles, the first one in flushing ran out of business because they were literally operating at a loss)

_I think they relocated to Bowery_

### Korean
- Baekjeong
- BCD (it's actually everywhere)
- TofuTofu (Bowery)

### Mexican
Pretty much non existant

- Paquito's
- maybe try visiting Jamaica, expect to pay double for half the quality
- Chipotle?!? is that even mexican

### Pizza
- Prince Street Pizza, gotta get that pepperoni
- Roberta's
- Hester's, if you're in chinatown and looking for an 8$ pie before heading home
- Costco, they lay that mozza on thicc, 1lb per 10$ pie

### Miscellaneous
- [Restuarant Week](https://www.nycgo.com/restaurant-week-about), July 23
- Pomme Fritz (poutine & thicc fries + dip)
- The Boil

### kick back and relax
I got you man! Plantain chips, get'em at

- any mom and pop store in Washington Heights
- Trader Joes (fun fact: the one on 14th street is the only one in the state able to sell wine)

_their pomegranate juice is the best shit ever, and it's doubles as an excellent chaser_

_Got a sweet tooth? Grab some scandinavian swimmers and a box of hold the cone!_



## **Activities**

### Bouldering
- [Brooklyn Boulders](https://brooklynboulders.com/gowanus/)

### Bowling
- [Shell Lanes](http://shelllanes.com/)
- [Bowlero Queens](https://www.bowlero.com/location/bowlero-queens)

### Mini-Golf
- [Pier 25 Minigolf](http://www.manhattanyouth.org/pier-25.aspx)

### Skating
- Golconda Playground
- Hudson River Park
- LES
- Coleman
- McCarren

### Hiking
both are fairly far away

- Anthony's Nose
- Fire Island
- use AllTrails to look up trails

### Street Ball
- NYC famous courts
	- W4 courts 
	- Dyckman Park 
- any park with a court

### Books
- [Strand](https://www.strandbooks.com/)
- [Kinokuniya](https://usa.kinokuniya.com/stores-kinokuniya-new-york])

### Co-working 
- [AWS pop-up loft](https://aws.amazon.com/start-ups/loft/ny-loft/), a place with free food, drinks, gigabit internet, and cheap mexican candy
- Capital One on 14th by union square (the one with peet's coffee inside, 1/2 off coffee with capital one card)
- plenty of other places, think WeWork

### Boardgames
- [Uncommons, a boardgame cafe near w4](http://uncommonsnyc.com/)
- Tribeca Wholefoods 

### Movies/Shows
- [Lincoln Center](http://www.lincolncenter.org/), free-for-all
- [free park movies](https://www.nycgovparks.org/events/free_summer_movies]) 
	- Bryant Park (Mondays)
	- Hudson River Park (Tuesdays and Wednesdays)
- [Cinema Film Noir](https://www.filmnoircinema.com/)
- [Cinemart](https://www.cinemartcinemas.com/), 8$ movies with free popcorn and free drinks on Tuesdays located in Forest Hills

### People watching
- Washington Square
- Union Square 
- Bryant park
- we got a lot of parks here

### Street Performance (singers, artists, dancers)
subway stations

- 42nd
- Union Square
- West 4th
- Herald Square
- Brooklyn Bridge


### Miscellaneous

Museums: We have all of them and they're all good.

Shopping: Just go to SoHo, you'll probably find it there.

Thrifting: 23rd between 3rd and 2nd is a slew of thrift stores, also try east village.

[Smorgasburg](https://www.smorgasburg.com/): weekend flea market in Brooklyn.

[Chelsea Market & Highline](http://www.chelseamarket.com/): pretty over-rated mall with a cool park/trail.